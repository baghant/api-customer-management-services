package com.customermanagement.baghant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCustomerManagementServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCustomerManagementServicesApplication.class, args);
	}
}
