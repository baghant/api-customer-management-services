/**
 * 
 */
package com.customermanagement.baghant.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customermanagement.baghant.service.RegistrationService;

/**
 * @author Bijendra
 *
 */
@RestController
@RequestMapping(value = "/baghant/customermanagement")
public class RegistrationController {

	@Autowired
	private RegistrationService registrationService;
	
	@RequestMapping(value = "/registry")
    public String register()
    {
        return registrationService.doRegistration("");
    }
}
