/**
 * 
 */
package com.customermanagement.baghant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.customermanagement.baghant.dao.RegistrationDao;

/**
 * @author Bijendra
 *
 */
@Service
public class RegistrationServiceImpl implements RegistrationDao {

	@Autowired
	private RegistrationDao registrationDao;
	
	public String doRegistration(String register) {
		return registrationDao.doRegistration("");
	}
}
