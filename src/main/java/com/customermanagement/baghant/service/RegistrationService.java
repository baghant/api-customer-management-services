/**
 * 
 */
package com.customermanagement.baghant.service;

/**
 * @author Bijendra
 *
 */
public interface RegistrationService {

	public String doRegistration(String register);
}
