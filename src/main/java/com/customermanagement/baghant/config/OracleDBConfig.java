/**
 * 
 */
package com.customermanagement.baghant.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import lombok.Data;

/**
 * @author Bijendra
 *
 */
@Configuration
@EnableConfigurationProperties(value={OracleDBConfig.OracleDataSourceProps.class})
public class OracleDBConfig {

	@Data
	@ConfigurationProperties(prefix="oracle.database")
	public class OracleDataSourceProps {
		private String driverClass;
		private String jdbcUrl;
		private String userName;
		private String password;
	}
	
	@Autowired
	private OracleDataSourceProps oracleDS;
	
	@Bean
	public  NamedParameterJdbcTemplate oracleNamedTemplate(){
		return new NamedParameterJdbcTemplate(getBasicDatasource());
	}
	
	@Bean
	public DataSource getBasicDatasource(){
		
		BasicDataSource datasource=new BasicDataSource();
		datasource.setUrl(oracleDS.getJdbcUrl());
		datasource.setDriverClassName(oracleDS.getDriverClass());
		datasource.setUsername(oracleDS.getUserName());
		datasource.setPassword(oracleDS.getPassword());
		
		return datasource;
	}
	
}
