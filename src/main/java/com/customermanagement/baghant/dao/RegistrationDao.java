/**
 * 
 */
package com.customermanagement.baghant.dao;

/**
 * @author Bijendra
 *
 */
public interface RegistrationDao {

	public String doRegistration(String registry);
}
