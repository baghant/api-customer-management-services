/**
 * 
 */
package com.customermanagement.baghant.dao.rowmapper;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;

/**
 * @author Bijendra
 *
 */
public class RegistrationRowMapper implements PreparedStatementCallback<Integer>{

	@Override
	public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
		
		return ps.executeUpdate();
	}

	

	

}
