/**
 * 
 */
package com.customermanagement.baghant.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.customermanagement.baghant.dao.rowmapper.RegistrationRowMapper;

/**
 * @author Bijendra
 *
 */
@Repository
public class RegistrationDaoImpl implements RegistrationDao {

	@Autowired
	private NamedParameterJdbcTemplate template;

	public String doRegistration(String registry) {
		final MapSqlParameterSource source = new MapSqlParameterSource();
		source.addValue("Name", registry);
		Integer id = template.execute("", source, new RegistrationRowMapper());
		return id.toString();
	}
}
